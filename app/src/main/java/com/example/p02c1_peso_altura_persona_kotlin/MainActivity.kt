package com.example.p02c1_peso_altura_persona_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var txtAltura: EditText
    private lateinit var txtPeso: EditText
    private lateinit var lblResultado: TextView
    private lateinit var btnCalcular: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnSalir: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main);
        txtAltura = findViewById(R.id.txtAltura);
        txtPeso = findViewById(R.id.txtPeso);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnSalir = findViewById(R.id.btnSalir);
        lblResultado = findViewById(R.id.lblResultado);

        btnCalcular.setOnClickListener {
            val ALTURA: Double
            val PESO: Double
            val IMC: Double
            if (txtAltura.getText().toString().equals("") || txtPeso.getText().toString().equals("")){
                Toast.makeText(applicationContext, "Favor de ingresar el Peso y/o la Altura", Toast.LENGTH_SHORT).show()
            } else {
                ALTURA = txtAltura.getText().toString().toDouble()
                PESO = txtPeso.getText().toString().toDouble() // Corrección aquí
                IMC = PESO / (ALTURA * ALTURA)
                lblResultado.setText("El IMC es: " + String.format("%.2f", IMC) + " :o")
            }
        }
        btnLimpiar.setOnClickListener {
            txtAltura.setText("")
            txtPeso.setText("")
            lblResultado.setText(".... --- .-.. .-  / .-- . -. .- ... ")
        }
        btnSalir.setOnClickListener { finish() }
    }
}